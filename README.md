# Learn Regex

## *Under Construction*

This is a game to help teach you RegEx in an intuitive and easy to understand way.

* https://gwenf.github.com/regex-tutor


## Running locally

1. `npm install`
1. `npm start`


## Project Roadmap

### Phase 1: MVP

 * [x] Settle on a theme for the game - Regex Breakfast
 * [ ] Have a basic list of words to match against
 * [ ] Have a basic list of words NOT to match against
 * [ ] Have a system that changes the view after each chapter
 * [x] Create a navigation for all of the chapters
 * [ ] Basic styles
 * [ ] Themed styles
 * [ ] Iconography/Meta Data

### Phase 2: Non-MVP
 * [ ] Buy a domain name?
