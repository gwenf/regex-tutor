//TODO: reorganize and split up this file
var arraysOfStrings = require('./strings.js');

var currentStringIndex = 0,
	wordListIndex = 0;
var currentWordList = arraysOfStrings.wordLists[currentStringIndex];
var strToMatch = document.getElementById('string-to-match');
strToMatch.innerText = arraysOfStrings[currentWordList][currentStringIndex];

var navMenuWordList = document.getElementsByClassName('word-list-nav-item');
console.log(document.getElementsByClassName('word-list-nav-item'))

function regexChange(e){
	var userInput = reInput.value;

	var isValid = true;
	try {
	    new RegExp(userInput);
	} catch(e) {
	    isValid = false;
	}

	if (isValid){
		var re = new RegExp(userInput);
		var highlightedStr = arraysOfStrings[currentWordList][currentStringIndex].replace(re, function(val){
			return '<span class="highlighted2">' + val + '</span>';
		});
		strToMatch.innerHTML = highlightedStr;

		// test if regex matches the whole string, not just one part
		var userInputLine = '^' + userInput + '$';
		var wholeLineRe = new RegExp(userInputLine);
		if (arraysOfStrings[currentWordList][currentStringIndex].match(wholeLineRe)){

			strToMatch.innerText = arraysOfStrings[currentWordList][currentStringIndex + 1];
			reInput.value = '';

			if (currentStringIndex===arraysOfStrings[currentWordList].length-1){
				currentStringIndex = 0;
				wordListIndex++;
				currentWordList = arraysOfStrings['wordLists'][wordListIndex];

				if (currentWordList==='modifiers'||currentWordList==='advanced'){
					document.getElementById('regex-modifiers').disabled = false;
				} else {
					document.getElementById('regex-modifiers').disabled = true;
				}
			} else {
				currentStringIndex++;
			}

			//updating navigation when challenge changes
			console.log(arraysOfStrings[currentWordList])
			var currentItem = document.getElementById(arraysOfStrings[currentWordList][currentStringIndex].replace(' ','-'));
			[].forEach.call(navMenuWordList, function(el) {
		        el.classList.remove('current');
		    });
			currentItem.classList += ' current';

			var allNavLists = document.getElementsByClassName('accordion-body');
			[].forEach.call(allNavLists, function(el) {
		        el.classList.remove('open');
		    });
			var currentList = document.getElementById(currentWordList);
			currentList.classList += ' open';
		}
	}
}

var reInput = document.getElementById("regex-main");
reInput.addEventListener("keyup", regexChange, false);


function updateCurrentChallenge(e){
	arraysOfStrings.wordLists.map(function(list){
		arraysOfStrings[list].map(function(str){
			if (arraysOfStrings[list][arraysOfStrings[list].indexOf(str)]===e.target.textContent){
				currentStringIndex = arraysOfStrings[list].indexOf(str);
				console.log(currentStringIndex, list)
				currentWordList = list;
				if (currentWordList==='modifiers'||currentWordList==='advanced'){
					document.getElementById('regex-modifiers').disabled = false;
				} else {
					document.getElementById('regex-modifiers').disabled = true;
				}
			}
		})
	})

	// update the DOM with new challenge based on chosen nav item
	strToMatch.innerText = arraysOfStrings[currentWordList][currentStringIndex];
}

for (var i=0; i<=navMenuWordList.length; i++){
	navMenuWordList[i].addEventListener('click', updateCurrentChallenge, false);
}






