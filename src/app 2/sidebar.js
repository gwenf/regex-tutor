var arraysOfStrings = require('./strings.js');
var accordionHeaders = document.querySelectorAll('.accordion-header');

function handleExpand(e){
	var accBody = e.target.nextElementSibling;
	accBody.classList.toggle('open');
}

function selectCurrentNavItem(e){
    [].forEach.call(navListItems, function(el) {
        el.classList.remove('current');
    });
	this.classList += ' current';

	// currentItemIndex = navListItems.indexOf(this);
	for (var item in navListItems){
		if (this===navListItems[item]){
			currentItemIndex = item;
		}
	}
}

function generateSideNavMenuItems(){
	arraysOfStrings.wordLists.map(function(wordList){
		var el = document.getElementById(wordList);
		var html = '';
		arraysOfStrings[wordList].map(function(word){
			html += '<li id="' + word.replace(' ','-') + '" class="word-list-nav-item">' + word + '<li>';
		})
		html = '<ol id="challenge-section-1" class="challenge-nav-group">' + html + '</ol>';
		el.innerHTML = html;
	})
}
generateSideNavMenuItems();

for (var i=0; i<accordionHeaders.length; i++){
	accordionHeaders[i].addEventListener('click', handleExpand, false);
}

var navListItems = document.querySelectorAll('.word-list-nav-item');
navListItems[0].classList += ' current';
var currentItemIndex = 0;

for (var i=0; i<navListItems.length; i++){
	navListItems[i].addEventListener('click', selectCurrentNavItem, false);
}

//dynamically update the sidebar as user advances in challenges
//write function to import into regex file? or maybe index.js... no no no not this
