import arraysOfStrings from './strings'

var currentStringIndex = 0,
	wordListIndex = 0,
	currentWordList = arraysOfStrings.wordLists[currentStringIndex];

var strToMatch = document.getElementById('string-to-match');
strToMatch.innerText = arraysOfStrings[currentWordList][currentStringIndex];

export var reInput = document.getElementById("regex-main");

export function regexChange(e){
	var userInput = reInput.value;

	var isValid = true;
	try {
	    new RegExp(userInput);
	} catch(e) {
	    isValid = false;
	}

	if (isValid){
		var re = new RegExp(userInput);
		var highlightedStr = arraysOfStrings[currentWordList][currentStringIndex].replace(re, function(val){
			return '<span class="highlighted2">' + val + '</span>';
		});
		strToMatch.innerHTML = highlightedStr;

		// test if regex matches the whole string, not just one part:
		var userInputLine = '^' + userInput + '$';
		var wholeLineRe = new RegExp(userInputLine);
		if (arraysOfStrings[currentWordList][currentStringIndex].match(wholeLineRe)){

			strToMatch.innerText = arraysOfStrings[currentWordList][currentStringIndex + 1];
			reInput.value = '';

			if (currentStringIndex===arraysOfStrings[currentWordList].length-1){
				currentStringIndex = 0;
				wordListIndex++;
				currentWordList = arraysOfStrings['wordLists'][wordListIndex];

				if (currentWordList==='modifiers'||currentWordList==='advanced'){
					document.getElementById('regex-modifiers').disabled = false;
				} else {
					document.getElementById('regex-modifiers').disabled = true;
				}
			} else {
				currentStringIndex++;
			}

		}
	}
}