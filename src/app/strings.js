var basicWords = [
	'fruit',
	'juice',
	'bacon and eggs'
]
var excapingWords = [
	'good\morning'
]
var modifierWords = [
	'bananas and eggs and toast and more bananas'
]
var advancedWords = [
	'morning@meal.com'
]
var wordLists = [
	'basics',
	'escaping',
	'modifiers',
	'advanced'
]

var categories = {
	basics: basicWords,
	escaping: excapingWords,
	modifiers: modifierWords,
	advanced: advancedWords,
	wordLists
}

export default categories;

