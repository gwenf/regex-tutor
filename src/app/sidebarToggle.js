var toggleBtn = document.querySelector('#sidebar-toggle-button');
var sidebar = document.querySelector('#challenge-sidebar');

toggleBtn.addEventListener('click', function() {
  toggleBtn.classList.toggle('is-closed');
  sidebar.classList.toggle('is-closed');
})