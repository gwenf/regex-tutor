/* SETTINGS */

var destination = 'dist';
var port = 8000;
var env = process.env.NODE_ENV || "development";


/* IMPORTS */
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var insert = require('gulp-insert');
var jshint = require('gulp-jshint');
var livereload = require('gulp-livereload');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');


/* TASKS */

// Refresh page when HTML files are updated
gulp.task('html', function () {
    gulp.src('*.html')
        .pipe(livereload());
});

// Create expanded and .min versions of styles
gulp.task('sass', function () {
    gulp.src('src/sass/index.sass')
        .pipe(sass({ outputStyle: 'expanded' })
        .on('error', sass.logError))
        .pipe(gulp.dest(destination))
        .pipe(rename('index.min.css'))
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest(destination))
        .pipe(livereload());
});

// Lint JS
gulp.task('lint', function() {
    return gulp.src('src/app/index.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});


gulp.task('js', function () {
    var bundler = browserify({
        entries: './src/app/index.js',
        debug: true
    });
    bundler.transform(babelify.configure({
        presets: ["es2015", "stage-0"]
    }));

    bundler.bundle()
        .on('error', function (err) { console.error(err); })
        .pipe(source('index.js'))
        // .pipe(sourcemaps.init({ loadMaps: true })) //TODO: want to fix source-maps
        //.pipe(uglify()) //add gulpif()???
        .pipe(rename('bundle.min.js'))
        // .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(destination))
        .pipe(livereload());
});

// Monitor for file changes and run tasks
gulp.task('watch', function () {
    gulp.watch('src/app/*.js', ['lint']);
    gulp.watch('src/app/*.js', ['js']);
    gulp.watch('src/sass/*.sass', ['sass']);
    gulp.watch('*.html', ['html']);

    livereload.listen();
});

// Run a local server
gulp.task('serve', function (done) {
    var express = require('express');
    var app = express();
    //path to the folder that will be served. __dirname is project root
    var path = __dirname;
    app.use(express.static(path));
    app.listen(port, function () {
         done();
    });
});

// Auto-open your default browser to the correct page
gulp.task('open', function () {
    var url = 'http://localhost:' + port;
    var OS = process.platform;
    var exectuable = '';

    //OS Specific values for opening files.
    if (OS == 'darwin') { executable = 'open ';     }
    if (OS == 'linux' ) { executable = 'xdg-open '; }
    if (OS == 'win32' ) { exectuable = 'explorer '; }

    //Run the OS specific command to open the url in the default browser
    require('child_process').exec(exectuable + url);
});


/* TASK GROUPS */

// Default task
gulp.task('default', ['sass', 'lint', 'js', 'watch', 'serve', 'open']);
